- Prio:
    - "reboot" command as modutilities
    - Modutil to add new roles to role-react command
    - Play music from youtube in voice channel
        - Search on youtube for the exact video, choose the first one
        - Alternatively, queue a video link directly
        - More search options/destinations later
- Modular "allow X role for command Y"
- Pass arguments into visitor-pattern-y functions to avoid so much indenting (not done for the rest of the commands. Not sure if needed)
- Adhere to commands more rigorously. Match command more tightly, instead of just "startswith()" (main.py on_message)
- Print Picture (database) ID's along with the picture
- Allow specific pictures to be requested, by their database ID
- Make a secret "bad bot" command against Philip, hehe
- A horny jail command
- Add "help" calls for commands. How to use the command and what it does. See decorators? :eyes:
- Add quote utilities
    - Remove
    - Update
    - Undo
- Save quote author's as numbers instead of strings
    - Add a path in the id fetcher to handle the author being a number
    - And make sure to only take that if the author is a number
- Maybe do some actual software design and consider using some kind of decorator to add commands
	- This would probably also clean the code quite a bit and make it much more maintainable tbhhh
	- Maybe just try out the library's decorator. Could be it's really good :)
- Automatically post "patch notes" when Froppy starts, if such is demanded by the settings

To make quote work for multiple servers:
	When a quote command comes in
		Select quotes only from the source guild
		Select based on normal arguments, if given

	When inputting
		Select quotes only from the source guild
		Count the number of quotes -> new quote id (since quotes are 0-indexed)
		insert normally
