from froppyhelpers import get_setting, update_setting


def bot_utilities_command(command):
    if command == "botutil":
        return True
    else:
        return False


def bot_utilities_help():
    return "`botutil`: A command to do stuff like enable/disable commands"


async def bot_utilities_response(arguments, message):
    # botutil [enable/disable] command [commandname]
    # All commands are enabled by default
    return await message.channel.send(argument_one(arguments, message))


def argument_one(arguments, message):
    if len(arguments) == 0:
        return "Please supply some arguments for this command."

    if arguments[0] == "command":
        return command(arguments[1:], message)
    else:
        return f"{arguments[0]} is not a valid argument for this command!"


def command(arguments, message):
    if len(arguments) == 0:
        return "You must call this command with `enable` or `disable` and the name of a different command"

    if arguments[0] == "enable":
        return command_enable(arguments[1:], message)
    elif arguments[0] == "disable":
        return command_disable(arguments[1:], message)
    else:
        return f"{arguments[0]} is not a valid state for a command. Use enable or disable."


def command_enable(arguments, message):
    if len(arguments) == 0:
        return "Please supply a command name to enable."
    elif arguments[0] == "botutil":
        return "Don't mess with this command."

    try:
        setting = get_setting(str(message.guild.id), "commands", arguments[0])
        if setting:
            return "That command is already enabled!"
        else:
            update_setting(str(message.guild.id), "commands", arguments[0], True)
            return "Command enabled!"
    except KeyError:
        return f"The command {arguments[0]} does not exist!"


def command_disable(arguments, message):
    if len(arguments) == 0:
        return "Please supply a command name to disable."
    elif arguments[0] == "botutil":
        return "Don't mess with this command."

    try:
        setting = not get_setting(str(message.guild.id), "commands", arguments[0])
        if setting:
            return "That command is already disabled!"
        else:
            update_setting(str(message.guild.id), "commands", arguments[0], False)
            return "Command disabled!"
    except KeyError:
        return f"The command {arguments[0]} does not exist!"
