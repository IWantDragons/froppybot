import pymongo
import platform
import os
from os import listdir
from os.path import isfile, join
import json


async def reload_all(bot, settings, command_names):
    print("Reloading all settings...")
    print("\tAdding missing Froppy pictures filepaths...")
    UpdateFroppyPicDB()
    print("\tDone!")
    print("\tUpdating general settings...")
    await UpdateSettings(bot, settings, command_names)
    print("\tDone!")
    print("Everything updated!")


def GetCommandFunctions(global_mods, local_dir):
    temp_list = []
    for module in list(filter(lambda x: x.endswith("_module"), local_dir)):
        temp_list.append(list(
                filter(lambda x: x.endswith("_command"),
                       dir(global_mods[module]))))

    command_names = []
    for sublist in temp_list:
        for item in sublist:
            command_names.append(item[:-8])
    return command_names


def UpdateFroppyPicDB():
    client = pymongo.MongoClient(host="localhost", port=27017)
    db = client["froppy"]
    collection = db["pictures"]
    local_paths = GetAllLocalFroppyPicPaths()
    db_paths = GetAllDBFroppyPicPaths(collection)
    paths_diff = list_diff(local_paths, db_paths)
    db_paths_length = len(db_paths)

    if paths_diff:
        print("There were some new pictures!")
        for i in range(len(db_paths), len(local_paths)):
            if collection.find_one({"Filepath": paths_diff[i-db_paths_length]}) is None:
                collection.insert_one({"PictureID": i, "Filepath": paths_diff[i-db_paths_length]})


def GetAllLocalFroppyPicPaths():
    if "MANJARO" in platform.release():
        directory = "/media/Dank_memes/froppy_pictures"
    else:
        directory = "/home/ubuntu/Froppybot/froppypictures"

    filenames = [f for f in listdir(directory) if isfile(join(directory, f))]
    abs_paths = []
    for name in filenames:
        abs_paths.append(os.path.abspath(os.path.join(directory, name)))
    return abs_paths


def GetAllDBFroppyPicPaths(collection):
    return [path["Filepath"] for path in list(collection.find({}, {"_id": 0, "PictureID": 0}))]


def list_diff(list_one, list_two):
    return [item for item in list_one + list_two if item not in list_one or item not in list_two]


async def UpdateSettings(bot, settings, command_names):
    print("\tAdding missing guilds to settings file, if any are found...")
    await add_missing_guilds(settings, bot)
    print("\tDone!")
    print("\tAdding missing commands to known guilds, if any are found...")
    add_missing_command_settings(settings, command_names)
    print("\tDone!")
    print("\tAdding missing reactions to react-role messages, if any are found...")
    await add_missing_reactions(settings, bot)
    print("\tDone!")
    print("\tWriting updated settings to file!")
    with open("settings.json", "w") as settings_file:
        json.dump(settings, settings_file, sort_keys=True, indent=4)
    print("\tDone!")


async def add_missing_guilds(settings, bot):
    guild_ids = [str(guild.id) for guild in await bot.fetch_guilds().flatten()]
    setting_ids = [id for id in settings]
    for id in list_diff(guild_ids, setting_ids):
        settings.update({str(id): {"prefix": "f! "}})


def add_missing_command_settings(settings, command_names):
    setting_ids = [id for id in settings]
    for id in setting_ids:
        try:
            settings[id]["commands"]
        except KeyError:
            settings[id].update({"commands": {}})
        for name in command_names:
            try:
                settings[id]["commands"][name]
            except KeyError:
                settings[id]["commands"].update({name: True})


async def add_missing_reactions(settings, bot):
    setting_ids = [id for id in settings]
    for id in setting_ids:
        try:
            guild = bot.get_guild(int(id))
            role_channel_id = int(settings[id]["role_react_channel"])
            role_message_id = int(settings[id]["role_react_message"])
            role_message = await guild.get_channel(role_channel_id).fetch_message(role_message_id)
            current_bot_reactions = [str(reaction.emoji.id) for reaction in role_message.reactions if reaction.me]
            desired_bot_reactions = [emoji_id for emoji_id in settings[id]["react_roles"]]
            missing_reactions = list_diff(current_bot_reactions, desired_bot_reactions)
            if missing_reactions:
                for emoji_id in missing_reactions:
                    emoji = await guild.fetch_emoji(int(emoji_id))
                    await role_message.add_reaction(emoji)
        except KeyError:
            pass


# No error catching
def get_setting(*path):
    with open("settings.json", "r") as settings_file:
        settings = json.load(settings_file)
        return rec_get_settings(settings, path)


def rec_get_settings(settings, pathtuple):
    path = list(pathtuple)
    if len(path) == 1:
        return settings[path[0]]
    else:
        temp = path.pop(0)
        return rec_get_settings(settings[temp], tuple(path))


def update_setting(*setting):
    with open("settings.json", "r+") as settings_file:
        settings = update_setting_rec(json.load(settings_file), setting)
        settings_file.truncate(0)
        settings_file.seek(0)
        json.dump(settings, settings_file, indent=4)


def update_setting_rec(setting_path, setting):
    settings_list = list(setting)
    if len(settings_list) == 1:
        return settings_list[0]
    else:
        temp = settings_list.pop(0)
        setting_path[temp] = update_setting_rec(setting_path[temp], tuple(settings_list))
        return setting_path


def add_setting(*setting):
    with open("settings.json", "r+") as settings_file:
        settings = add_setting_rec(json.load(settings_file), setting)
        settings_file.truncate(0)
        settings_file.seek(0)
        json.dump(settings, settings_file, indent=4)


def add_setting_rec(settings_path, setting):
    settings_list = list(setting)
    if len(settings_list) == 2:
        new_setting = {str(settings_list[0]): settings_list[1]}
        settings_path.update(new_setting)
        return settings_path
    else:
        temp = settings_list.pop(0)
        settings_path[temp] = add_setting_rec(settings_path[temp], tuple(settings_list))
        return settings_path
