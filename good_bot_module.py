def good_bot_command(command):
    if command == "good":
        return True
    else:
        return False


def good_bot_help():
    return "`good`: A command to tell people (or me) they are good"


async def good_bot_response(arguments, message):
    # TODO: Maybe turn "bot" into "me" when there's a mention of froppy or "bot"
    # in a multi-arg call.
    if len(arguments) == 0:
        return await message.channel.send("This command takes at least 1 argument!")
    elif len(arguments) == 1 and arguments[0] == "bot":
        return await message.channel.send("<:FroppyCute:353524281466028032> Thank you, kero. :heart:")
    elif len(message.mentions) == len(arguments):
        name_list = mentions_to_nick_list(message)
        mentions = " and ".join(name_list)
        return await message.channel.send(f"Good {mentions}!")
    else:
        filtered_args = list(dict.fromkeys(arguments))
        good_things = [arg for arg in filtered_args if not arg == "and"]
        print(good_things)
        if len(message.mentions) != 0:
            good_things = good_things + mentions_to_nick_list(message)
            for mention in message.mentions:
                good_things = [thing for thing in good_things if not str(mention.id) in thing]
        good_things = " and ".join(good_things)
        print(good_things)
        return await message.channel.send(f"Good {good_things}!")


def mentions_to_nick_list(message):
    return [message.guild.get_member(user.id).display_name for user in message.mentions]
