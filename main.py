import discord
import os
import froppyhelpers
import pymongo
import json
from mod_utilities_module import mod_utilities_response

# Tell Discord that we want to handle stuff with users
intents = discord.Intents.default()
intents.members = True
intents.reactions = True

# Do some funky hacking to import all the different modules.
# First, get the names of all files in the folder that end with "_module.py"
modules = list(filter(lambda x: x.endswith("_module.py"), os.listdir()))
# Then, for each of those...
for module in modules:
    # Call the import function to get the functionality and so that we can
    # fetch them in a little bit.
    globals()[module[:-3]] = __import__(module[:-3])

# If there's an easier way to do the above without it being a really hacky
# way to do it, please share.

# Some "statics"
print("Loading and importing module files...")
GLOBALS = globals()
COMMAND_NAMES = froppyhelpers.GetCommandFunctions(GLOBALS, dir())
with open("settings.json") as file:
    SETTINGS = json.load(file)
print("Done!\n")

# If there are new pictures in the folder that are not in the database, this
# will add them.
print("Updating froppy picture database...")
froppyhelpers.UpdateFroppyPicDB()
print("Done!\n")

# Count the pieces of content in the database.
print("Counting elements in picture and quote database...")
mongo_client = pymongo.MongoClient(host="localhost", port=27017)
db = mongo_client["froppy"]
collection = db["pictures"]
print(f"There are {collection.count_documents({})} pictures in DB")
collection = db["quotes"]
print(f"There are {collection.count_documents({})} quotes in DB")
mongo_client.close()
print("")

# The actual bot
bot = discord.Client(intents=intents)


@bot.event
async def on_ready():
    print("Logged on as {0}!".format(bot.user))
    print("")
    print("All loaded commands:")
    for item in COMMAND_NAMES:
        print(f"\t- {item}")
    print("")
    print("Updating settings...")
    # settings are updated late, since it depends on which servers the bot
    # is part of.
    await froppyhelpers.UpdateSettings(bot, SETTINGS, COMMAND_NAMES)
    print("Done!")
    print("\nAll done! Ready to kero!")


@bot.event
async def on_message(message):
    guild_prefix = SETTINGS[str(message.guild.id)]["prefix"]
    # Ignore all messages that are not of interest to the bot.
    if message.author != bot.user and message.content.startswith(guild_prefix):
        await message_handler(message)


async def message_handler(message):
    # For debugging and logging purposes
    print(f"Message from '{message.author}' (nick: '{message.author.nick}') in server '{message.guild.name}': {message.content}")

    # Update settings. Probably overkill to do it on every command, but better
    # safe than sorry. The only thing that doesn't update, is if the prefix
    # was changed directly in the file. Then, there has to be 1 command with
    # the old prefix.
    with open("settings.json") as file:
        global SETTINGS
        SETTINGS = json.load(file)
    # Process the command a bit. Remove the prefix from the message,
    # yoink the specific command, and make a list of the arguments
    guild_prefix = SETTINGS[str(message.guild.id)]["prefix"]
    arguments = message.content[len(guild_prefix):].split()
    if len(arguments) == 0:
        return await message.channel.send("Please include a command, kero!")
    command = arguments.pop(0)

    # Special case for mod utilities, since it needs a lot of "secret" stuff to work.
    if command == "modutil":
        return await message.channel.send(await mod_utilities_response(arguments, message, bot, SETTINGS, COMMAND_NAMES))

    # for if you forget a command
    if command == "help":
        return await message.channel.send(get_command_helps(arguments, message))

    # Default response
    response = "I don't recognize that command, kero!"
    # More sort of hacky stuff.
    for name in COMMAND_NAMES:
        # Build the module name
        module = name + "_module"
        # Get the method that answers whether it applies to the module
        method = getattr(GLOBALS[module], name + "_command")
        # Then execute it and see what it says
        if method(command):
            # If it's the right command, check if it's enabled on the server
            # the message came from
            if not command_enabled(name, message):
                return await message.channel.send("That command is disabled on this server.")
            if not user_can_use(name, message):
                return await message.channel.send("This is a mod-only command.")
            # If everything checks out so far, get the response method and
            # execute it
            response_method = getattr(GLOBALS[module], name + "_response")
            return await response_method(arguments, message)
    # If none of that works, it falls back to sending the default response
    return await message.channel.send(response)


# Have to use raw reacts, since role message may be out of message cache
# If message is out of message cache, the fancy on_reaction_add is not called.
@bot.event
async def on_raw_reaction_add(payload):
    with open("settings.json") as file:
        global SETTINGS
        SETTINGS = json.load(file)
    module = GLOBALS["role_reaction_handler_module"]
    handle_check_method = getattr(module, "role_reaction_should_handle")
    if handle_check_method(payload, SETTINGS):
        handler_method = getattr(module, "role_reaction_handle")
        await handler_method(payload, SETTINGS, bot)


@bot.event
async def on_raw_reaction_remove(payload):
    with open("settings.json") as file:
        global SETTINGS
        SETTINGS = json.load(file)
    module = GLOBALS["role_reaction_handler_module"]
    handle_check_method = getattr(module, "role_reaction_should_handle")
    if handle_check_method(payload, SETTINGS):
        handler_method = getattr(module, "role_reaction_handle")
        await handler_method(payload, SETTINGS, bot)


def get_command_helps(arguments, message):
    returnstring = ""
    if len(arguments) != 0:
        return "Sorry, no specific command help yet"
    for name in COMMAND_NAMES:
        if SETTINGS[str(message.guild.id)]["commands"][name]:
            module = name + "_module"
            help_method = getattr(GLOBALS[module], name + "_help")
            returnstring += help_method() + '\n'
    return returnstring


def command_enabled(name, message):
    try:
        return SETTINGS[str(message.guild.id)]["commands"][name]
    except KeyError:
        return False


def user_can_use(name, message):
    try:
        mod_roles = SETTINGS[str(message.guild.id)]["mod_roles"]
        author_roles = [role.id for role in message.author.roles]
        guild_id = str(message.guild.id)
        # List intersection. The user should have a role that intersects
        # with mod roles if they're allowed to use the command.
        if (SETTINGS[guild_id]["mod_only"][name]
           and [item for item in mod_roles if item in author_roles]):
            return True
        else:
            return False
    except KeyError:
        return True


# Run the bot.
with open("SECRET_TOKEN_DO_NOT_ADD_TO_GIT.txt", "r") as tokenfile:
    token = tokenfile.read()
print("Logging in...")
bot.run(token)
