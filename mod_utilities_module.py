from froppyhelpers import update_setting, reload_all, add_missing_reactions, add_setting
import json


def mod_utilities_command(command):
    if command == "modutil":
        return True
    else:
        return False


def mod_utilities_help():
    return "`modutil`: A command for mods to set up sensitive stuff"


async def mod_utilities_response(arguments, message, bot, settings, command_names):
    return await argument_one(arguments, message, bot, settings, command_names)


async def argument_one(arguments, message, bot, settings, command_names):
    if len(arguments) == 0:
        return "Please supply some arguments for this command!"

    if arguments[0] == "restart":
        async with message.channel.typing():
            with open("settings.json") as file:
                settings = json.load(file)
                await reload_all(bot, settings, command_names)
        return "All settings updated!"
    elif arguments[0] == "update":
        return await argument_two(arguments[1:], message, settings, bot)


async def argument_two(arguments, message, settings, bot):
    if len(arguments) == 0:
        return "Please supply some setting to update"

    if arguments[0] == "role_react":
        return await role_react_update(arguments[1:], message, settings, bot)

    else:
        return "Something went wrong"


async def role_react_update(arguments, message, settings, bot):
    if len(arguments) == 0:
        return "Please supply some setting regarding role reactions to update"

    if arguments[0] == "message_id" and len(arguments) == 2:
        if not arguments[1].isdigit():
            return "Please provide a message ID"
        else:
            update_setting(str(message.guild.id), "role_react_message", int(arguments[1]))
            with open("settings.json") as file:
                upd_settings = json.load(file)
                await add_missing_reactions(upd_settings, bot)
            return f"Role react message ID updated to {arguments[1]}"
    elif arguments[0] == "channel_id" and len(arguments) == 2:
        if not arguments[1].isdigit():
            return "Please provide a channel ID"
        else:
            update_setting(str(message.guild.id), "role_react_channel", int(arguments[1]))
            return f"Role react channel ID updated to {arguments[1]}"
    elif arguments[0] == "add_react" and len(arguments) == 3:
        if not arguments[1].isdigit():
            return "Please provide an emoji ID"
        elif not arguments[2].isdigit():
            return "Please provide a role ID"
        else:
            add_setting(str(message.guild.id), "react_roles", str(arguments[1]), int(arguments[2]))
            with open("settings.json") as file:
                upd_settings = json.load(file)
                await add_missing_reactions(upd_settings, bot)
            return "Updated possible role reactions"
