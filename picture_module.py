import pymongo
import discord
import random


def picture_command(command):
    if command == "picture":
        return True
    else:
        return False


def picture_help():
    return "`picture`: A command to get a random picture of Froppy :)"


async def picture_response(arguments, message):
    if len(arguments) != 0:
        return await message.channel.send("This command doesn't take any arguments! (Yet)")

    mongo_client = pymongo.MongoClient(host="localhost", port=27017)
    db = mongo_client["froppy"]
    collection = db["pictures"]
    total_pictures = collection.count_documents({})
    random_id = random.randint(0, total_pictures)
    filepath = collection.find_one({"PictureID": random_id})["Filepath"]
    pic_file = open(filepath, "rb")
    mongo_client.close()
    return await message.channel.send(content="Here's a random picture", file=discord.File(pic_file))
