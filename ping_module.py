def ping_command(command):
    if command == "ping":
        return True
    else:
        return False


def ping_help():
    return "`ping`: A command to ping me"


async def ping_response(arguments, message):
    if len(arguments) != 0:
        return await message.channel.send("This command doesn't take any arguments (but I still received it :wink:)")

    return await message.channel.send("kero!")
