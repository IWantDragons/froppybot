from discord import HTTPException

def purge_command(command):
    if command == "purge":
        return True
    else:
        return False


def purge_help():
    return "`purge`: A command to purge messages!"


async def purge_response(arguments, message):
    # pattern: purge [1-9*] [@mention]?
    if len(arguments) == 0 or len(arguments) > 2:
        return await message.channel.send("This command takes an integer argument (for indiscriminate purging), or an integer and a mention (for discriminate purging)")
    elif len(arguments) == 1:
        return await message.channel.send(await purge_indiscriminate(arguments, message))
    elif len(arguments) == 2:
        return await message.channel.send(await purge_discriminately(arguments, message))


async def purge_indiscriminate(arguments, message):
    if not arguments[0].isdigit():
        return "The first argument must be an integer!"
    try:
        await message.channel.purge(limit=int(arguments[0]) + 1)
        return "Purged successfully"
    except HTTPException:
        return "There was an HTTP error! Check if the results are as desired..."


async def purge_discriminately(arguments, message):
    if not arguments[0].isdigit():
        return "The first argument must be an integer!"

    # Predicate: Author of a message is the mention, and (in case of self-mention)
    # the message invoking the purge is not the message being checked.
    message_pred = lambda x: x.author.id == message.mentions[0].id and x.id != message.id
    messages_to_delete = await get_messages_by_pred(message.channel, int(arguments[0]), message_pred)

    try:
        await message.channel.delete_messages(messages_to_delete)
        await message.delete()
        return "Purged successfully"
    except HTTPException:
        return "There was an HTTP error. Check if the results are as desired"


async def get_messages_by_pred(channel, limit, pred):
    return await get_messages_by_pred_rec(channel, limit, pred, [], 1)


async def get_messages_by_pred_rec(channel, lim, pred, messages, count):
    async for message in channel.history(limit=lim*count):
        if pred(message) and message not in messages:
            messages.append(message)
            if len(messages) == lim:
                return messages
    return await get_messages_by_pred_rec(channel, lim, pred, messages, count + 1)
