import pymongo
import random
import re
import time


def quote_command(command):
    if command == "quote":
        return True
    else:
        return False


def quote_help():
    return "`quote`: A command to quote people"


async def quote_response(args, message):
    # Gonna need this (?:[<(](.*?)[>)])?\s*(?:\"(.+)\"|([^#]+))?\s*(?:#(\d+))?
    mongo_client = pymongo.MongoClient(host="localhost", port=27017)
    db = mongo_client["froppy"]
    collection = db["quotes"]
    regex = re.compile("(?:[<(](.*?)[>)])?\s*(?:\"(.+)\"|([^#]+))?\s*(?:#(\d+))?")
    arguments = " ".join(args)
    matches = regex.match(arguments)
    groups = matches.groups()

    # Match groups:
    # 0 is a mention or name
    # 1 is "encapsulated quote"
    # 2 is keyword
    # 3 is a quote ID

    if check_content(groups):
        # random quote
        # We do -1 from total, since 'total_quotes' an actual count from 1 not 0.
        quotes_from_guild = collection.find({"guild_id": message.guild.id})
        total_quotes = quotes_from_guild.count()
        if total_quotes == 0:
            return await message.channel.send("No one in this server has been quoted yet!")
        quote = quotes_from_guild[random.randint(0, total_quotes - 1)]
        return await message.channel.send(make_quote_reply(quote, message))
    elif check_content(groups, 0):
        # Random quote from person
        quotes_from_person = collection.find({"guild_id": message.guild.id, "author": groups[0]})
        total_quotes = quotes_from_person.count()
        if total_quotes == 0:
            return await message.channel.send("That person hasn't been quoted yet!")
        else:
            quote = quotes_from_person[random.randint(0, total_quotes - 1)]
            return await message.channel.send(make_quote_reply(quote, message))
    elif check_content(groups, 0, 1):
        # Insert quote in DB
        content = groups[1]
        author = groups[0][2:]
        guild_id = message.guild.id
        timestamp = time.ctime()
        quoter = message.author.id
        id = collection.count_documents({"guild_id": message.guild.id})
        insert = {"id": id, "content": content, "author": author, "guild_id": guild_id, "timestamp": timestamp, "quoter": quoter}
        collection.insert_one(insert)
        return await message.channel.send(f"Recorded quote #{id} as *\"{content}\"*")
    elif check_content(groups, 0, 2):
        # Random quote from mention/person that contains keyword
        quotes_from_person = collection.find({"guild_id": message.guild.id, "author": groups[0]})
        quotes_with_word = [quote for quote in quotes_from_person if groups[2].lower() in quote["content"].lower()]
        total_quotes = len(quotes_with_word)
        if total_quotes == 0:
            return await message.channel.send("That person hasn't said anything with that search!")
        else:
            quote = quote = quotes_with_word[random.randint(0, total_quotes - 1)]
            return await message.channel.send(make_quote_reply(quote, message))
    elif check_content(groups, 2):
        # Random quote that contains keyword
        quotes_from_guild = collection.find({"guild_id": message.guild.id})
        quotes_with_word = [quote for quote in quotes_from_guild if groups[2].lower() in quote["content"].lower()]
        total_quotes = len(quotes_with_word)
        if total_quotes == 0:
            return await message.channel.send("No quotes exist with that search!")
        else:
            quote = quote = quotes_with_word[random.randint(0, total_quotes - 1)]
            return await message.channel.send(make_quote_reply(quote, message))
    elif check_content(groups, 3):
        # Quote with specific ID
        quote = collection.find_one({"guild_id": message.guild.id, "id": int(groups[3])})
        if not quote:
            return await message.channel.send("That ID doesn't exist (yet)!")
        else:
            return await message.channel.send(make_quote_reply(quote, message))
    else:
        # Something is wrong
        return await message.channel.send("That is an invalid combination of arguments :(")


# This is basically a mask check. Numbers are given for the position of what
# element is expected, and then we check if the element in the tuple
# corresponding to that position is found or not. If something is not as
# expected, False will be returned, but otherwise we'll "survive" the loop
# and return True
def check_content(groups, *numbers):
    for i in range(0, len(groups)):
        if groups[i] is not None:
            if i not in numbers:
                return False
        elif groups[i] is None:
            if i in numbers:
                return False

    return True


def make_quote_reply(quote, message):
    id = quote["id"]
    content = quote["content"]
    if quote["author"].startswith("@!"):
        author_id = int(quote["author"][2:])
        author = message.guild.get_member(author_id).display_name
    elif quote["author"].isdigit():
        author_id = int(quote["author"])
        author = message.guild.get_member(author_id).display_name
    else:
        author = quote["author"]
    return f"#{id}: *\"{content}\"*\n~ {author}"
