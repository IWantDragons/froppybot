def role_reaction_should_handle(payload, settings):
    try:
        if settings[str(payload.guild_id)]["role_react_message"] == payload.message_id:
            if payload.event_type == "REACTION_ADD":
                if not payload.member.bot:
                    return True
            else:
                return True
    except KeyError:
        return False


async def role_reaction_handle(payload, settings, bot):
    try:
        if (settings[str(payload.guild_id)]["role_react_message"] == payload.message_id
           and payload.event_type == "REACTION_ADD"):
            if str(payload.emoji.id) in settings[str(payload.guild_id)]["react_roles"]:
                await add_role_to_member(payload, settings, bot)
            else:
                await remove_invalid_react(payload, bot)
        elif (settings[str(payload.guild_id)]["role_react_message"] == payload.message_id
              and payload.event_type == "REACTION_REMOVE"):
            await remove_role_from_member(payload, settings, bot)
    except KeyError:
        pass


async def add_role_to_member(payload, settings, bot):
    guild = bot.get_guild(payload.guild_id)
    user = guild.get_member(payload.user_id)
    role_id = settings[str(payload.guild_id)]["react_roles"][str(payload.emoji.id)]
    role = guild.get_role(role_id)
    await user.add_roles(role, reason=f"Reacted with emoji '{payload.emoji.name}' to role reactions command")


async def remove_role_from_member(payload, settings, bot):
    guild = bot.get_guild(payload.guild_id)
    user = guild.get_member(payload.user_id)
    role_id = settings[str(payload.guild_id)]["react_roles"][str(payload.emoji.id)]
    role = guild.get_role(role_id)
    await user.remove_roles(role, reason=f"Reacted with emoji '{payload.emoji.name}' to role reactions command")


async def remove_invalid_react(payload, bot):
    emoji = payload.emoji
    user = bot.get_guild(payload.guild_id).get_member(payload.user_id)
    message = bot.get_guild(payload.guild_id).get_channel(payload.channel_id).get_partial_message(payload.message_id)
    await message.remove_reaction(emoji, user)
