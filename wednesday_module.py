from datetime import date


def wednesday_command(command):
    if command == "wednesday":
        return True
    else:
        return False


def wednesday_help():
    return "`wednesday`: A command to check if it is wednesday, my dudes"


async def wednesday_response(arguments, message):
    if len(arguments) != 0:
        return await message.channel.send("This command doesn't take any arguments!")
    # Check if it is wednesday
    if date.today().weekday() == 2:
        return await message.channel.send("<:HYPERS:753891153144709181> It is wednesday my dudes! <:HYPERS:753891153144709181>")
    else:
        return await message.channel.send("<:PepeWhy:418345287925366787> It is not wednesday. <:PepeWhy:418345287925366787>")
